package pl.codementors.servlet;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class CvDataStoreContext implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        if (servletContextEvent.getServletContext().getAttribute("store") != null) {
            throw new IllegalStateException("Cv store already exists");
        }
        CvDataStore store = new CvDataStore();
        store.init();
        servletContextEvent.getServletContext().setAttribute("store", store);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
