package pl.codementors.servlet;


import pl.codementors.servlet.model.WorkExperience;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "cvServlet", urlPatterns = {"/api/cv"})
public class CvServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String path = request.getPathInfo();
        if (path == null || "/".equals(path)) {
            getCV(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String path = request.getPathInfo();
        if (path == null || "/".equals(path)) {
            AddWorkExp(request, response);
        } else {
            super.doPost(request, response);
        }
    }

    private CvDataStore getStore() {
        return (CvDataStore) getServletContext().getAttribute("store");
    }

    private void getCV(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Jsonb jsonb = JsonbBuilder.create();
        String json = jsonb.toJson(getStore().getCv());
        response.setContentType("application/json; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().println(json);
    }

    private void AddWorkExp(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Jsonb jsonb = JsonbBuilder.create();
        WorkExperience workExperience = jsonb.fromJson(request.getReader(), WorkExperience.class);
        getStore().addWorkExp(workExperience);
    }
}
